#!/bin/bash
set -o allexport; source settings.env; set +o allexport

docker run -it --name ${IMAGE_NAME} --rm ${ORGANIZATION}/${IMAGE_NAME}:${VERSION} 
