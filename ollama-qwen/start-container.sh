#!/bin/bash
set -o allexport; source settings.env; set +o allexport

docker run -it -p 11434:11434 --name ${IMAGE_NAME} --rm ${ORGANIZATION}/${IMAGE_NAME}:${VERSION} 
