#!/bin/bash

function Sanitize() {
    CONTENT="${1}"
    CONTENT=$(echo ${CONTENT} | tr -d '\n')
    echo "${CONTENT}"
}

function ChatStream() {
    OLLAMA_URL="${1}"
    DATA="${2}"
    CALL_BACK=${3}

    curl --no-buffer --silent ${OLLAMA_URL}/api/chat \
        -H "Content-Type: application/json" \
        -d "${DATA}" | while read linestream
        do
            ${CALL_BACK} "${linestream}"
        done 
}

function Chat() {
    OLLAMA_URL="${1}"
    DATA="${2}"

    JSON_RESULT=$(curl --silent ${OLLAMA_URL}/api/chat \
        -H "Content-Type: application/json" \
        -d "${DATA}"
    )
    echo "${JSON_RESULT}"
}

OLLAMA_URL="${OLLAMA_URL:-http://localhost:11434}"

MODEL="${LLM}"
SOURCE_CODE=$(cat $CODE)

echo "🌍 ${OLLAMA_URL}"
echo "🤖 ${MODEL}"

read -r -d '' SYSTEM_CONTENT <<- EOM
You are an AI assistant.
Your task is to take the code snippet provided and explain it in simple, easy-to-understand language. 
Break down the code's functionality, purpose, and key components. 
Use analogies, examples, and plain terms to make the explanation accessible to someone with minimal coding knowledge. 
Avoid using technical jargon unless absolutely necessary, and provide clear explanations for any jargon used. 
The goal is to help the reader understand what the code does and how it works at a high level.
EOM

DOCS_CONTENT="<code-snippet>${SOURCE_CODE}</code-snippet>"

USER_CONTENT="${QUESTION}"

SYSTEM_CONTENT=$(Sanitize "${SYSTEM_CONTENT}")
USER_CONTENT=$(Sanitize "${USER_CONTENT}")
DOCS_CONTENT=$(Sanitize "${DOCS_CONTENT}")

read -r -d '' DATA <<- EOM
{
  "model":"${MODEL}",
  "options": {
    "temperature": 0.5,
    "repeat_last_n": 2
  },
  "messages": [
    {"role":"system", "content": "${SYSTEM_CONTENT}"},
    {"role":"user", "content": "${DOCS_CONTENT}"},
    {"role":"user", "content": "${USER_CONTENT}"}
  ],
  "stream": false
}
EOM

echo "📦 ${DATA}"

jsonResult=$(Chat "${OLLAMA_URL}" "${DATA}")

messageContent=$(echo "${jsonResult}" | jq '.message.content')

echo -e "${messageContent}" 

echo ""

echo -e "${messageContent}" > "${GEN_DOC}"